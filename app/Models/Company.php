<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
	public $timestamps = false;
	protected $fillable = [
		'name', 'active'
	];

	public function places()
	{
		return $this->hasMany('App\Models\Place');
	}
}
