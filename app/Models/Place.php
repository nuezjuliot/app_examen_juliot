<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
	
	public $timestamps = false;
	protected $fillable = [
		'name', 'company_id', 'active'
	];

	public function company(){
		return $this->belongsTo('App\Models\Company');
	}

}
