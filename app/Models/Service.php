<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'SERVICE';
	public $timestamps = false;
	protected $fillable = [
	     'name', 'preparation','time','place_id','is_enabled'
	];
}
