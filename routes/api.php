<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('company', 'CompanyController');
Route::resource('place', 'PlaceController');
Route::get('sucursales-de-empresa/{id_empresa}', 'CompanyController@getSucursalesDeEmpresa');